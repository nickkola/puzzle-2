-- Supported values for orientation:
-- portrait, portraitUpsideDown, landscapeLeft, landscapeRight

settings = {
	
	orientation = {
		default = "portrait",
		supported = { "portrait", }
	},
	
	iphone = {
		plist = {
			UIStatusBarHidden = false,
			UIPrerenderedIcon = true, -- set to false for "shine" overlay
			--UIApplicationExitsOnSuspend = true, -- uncomment to quit app on suspend
			
		
--Hide the Status Bar
display.setStatusBar ( display.HiddenStatusBar )

            --[[
            -- iOS app URL schemes:
            CFBundleURLTypes =
            {
                {
                    CFBundleURLSchemes =
                    {
                        "fbXXXXXXXXXXXXXX", -- example scheme for facebook
                        "coronasdkapp", -- example second scheme
                    }
                }
            }
            --]]
		}
	},
	
	--[[
	-- Android permissions

	androidPermissions = {
  		"android.permission.INTERNET",
  	},

	]]--
}
