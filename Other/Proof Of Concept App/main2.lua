-----------------------------------------------------------------------------------------
--
-- Code Snippet for the Puzzle POC
--
-----------------------------------------------------------------------------------------
display.setStatusBar( display.HiddenStatusBar )


----------------------------------
----------------------------------
--Get the HEIGHT & WIDTH of the Device's Screen
_SCREEN = {
		HEIGHT = display.contentHeight,
		WIDTH = display.contentWidth
}

--Get the CENTER POINT of the Device's Screen
_SCREEN.CENTER = {
	x = display.contentCenterX,
	y = display.contentCenterY
}

----------------------------------
----------------------------------
--Background Rectangle with White Fill
local bg = display.newRect( 0, 0, _SCREEN.WIDTH, _SCREEN.HEIGHT)
bg.x = _SCREEN.CENTER.x
bg.y = _SCREEN.CENTER.y
bg.setfillcolor = 1,1,1

--Display the Forrester Cohen Logo in the Middle
local logo = display.newImage("boardonly@2x.png",_SCREEN.CENTER.x + 7,_SCREEN.CENTER.y * 0.35)
logo.height = 190;
logo.width = 310;

----------------------------------
----------------------------------
--Logo Transition
transition.from(logo, {
	time = 1000,
	yScale = 0.1,
	xScale = 0.1,
	transition = easing.outBounce,
	})

transition.to(logo,{
		time = 1000,
		alpha = 1,
		transition = easing.outQuad,
	})
----------------------------------
----------------------------------
--Declare the Function of the TextBox

local emailtextbox

local function textListener( event )

    if ( event.phase == "began" ) then

        -- user begins editing text field
        print( event.text )

    elseif ( event.phase == "ended" or event.phase == "submitted" ) then

        -- text field loses focus
        -- do something with defaultField's text

    elseif ( event.phase == "editing" ) then

        print( event.newCharacters )
        print( event.oldText )
        print( event.startPosition )
        print( event.text )

    end
end

-- Create The Text Box
emailtextbox = native.newTextField( _SCREEN.CENTER.x, _SCREEN.CENTER.y * 1.5, 180, 30 )
emailtextbox.placeholder = "Email Address"
emailtextbox:addEventListener( "userInput", textListener )
emailtextbox.align = "center"
emailtextbox = display.newImage('bg_textbox.png', _SCREEN.CENTER.x, _SCREEN.CENTER.y * 1.49)
----------------------------------
----------------------------------
local submitgroup = display.newGroup()

local submitbutton = display.newRoundedRect(_SCREEN.CENTER.x, _SCREEN.CENTER.y * 1.71, 200, 60, 4)
submitbutton:setFillColor( 151/255, 196/255, 43/255 )

local submittext = display.newText( "Submit", _SCREEN.CENTER.x, _SCREEN.CENTER.y * 1.71, "Helvetica", 18 )
submittext:setTextColor( 0, 0, 0, 255 )

submitgroup:insert(submitbutton)
submitgroup:insert(submittext)

