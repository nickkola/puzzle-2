-----------------------------------------------------------------------------------------
--
-- Code Snippet for the Puzzle POC
--
-----------------------------------------------------------------------------------------
display.setStatusBar( display.HiddenStatusBar )


----------------------------------
----------------------------------
--Get the HEIGHT & WIDTH of the Device's Screen
_SCREEN = {
		HEIGHT = display.contentHeight,
		WIDTH = display.contentWidth
}

--Get the CENTER POINT of the Device's Screen
_SCREEN.CENTER = {
	x = display.contentCenterX,
	y = display.contentCenterY
}

----------------------------------
----------------------------------
--Background Rectangle with White Fill
local bg = display.newRect( 0, 0, _SCREEN.WIDTH, _SCREEN.HEIGHT)
bg.x = _SCREEN.CENTER.x
bg.y = _SCREEN.CENTER.y
bg.setfillcolor = 1,1,1

--Display the Forrester Cohen Logo in the Middle
local logo = display.newImage("boardonly@2x.png",_SCREEN.CENTER.x + 7,_SCREEN.CENTER.y * 0.35)
logo.height = 190;
logo.width = 310;

----------------------------------
----------------------------------
--Text Below Logo
local options = 
{
    --parent = textGroup,
    text = "Welcome to Forrester Cohens Puzzle App, Please Fill In Email Below In Order to Complete Your Submission",     
    x = _SCREEN.CENTER.x,
    y = _SCREEN.CENTER.y * 0.9,
    width = 170,     --required for multi-line and alignment
    font = native.systemFontBold,   
    fontSize = 18,
    align = "center"  --new alignment parameter
}

local myText = display.newText( options )
myText:setFillColor( 55/255, 51/255, 131/255 )

----------------------------------
----------------------------------
--Declare the Function of the TextBox [THIS MIGHT NEED TWEAKING & WILL NEED YOUR SPECIAL KEYBOARD ADDED]
local emailtextbox

local function textListener( event )

    if ( event.phase == "began" ) then

        -- user begins editing text field
        print( event.text )

    elseif ( event.phase == "ended" or event.phase == "submitted" ) then

        -- text field loses focus
        -- do something with defaultField's text

    elseif ( event.phase == "editing" ) then

        print( event.newCharacters )
        print( event.oldText )
        print( event.startPosition )
        print( event.text )

    end
end

-- Create The Text Box
emailtextbox = native.newTextField( _SCREEN.CENTER.x, _SCREEN.CENTER.y * 1.5, 180, 30 )
emailtextbox.placeholder = "Email Address"
emailtextbox:addEventListener( "userInput", textListener )
emailtextbox.align = "center"
emailtextbox = display.newImage('bg_textbox.png', _SCREEN.CENTER.x, _SCREEN.CENTER.y * 1.49)
----------------------------------
----------------------------------

--Create a Green Rectangle with Submit in it
local submitgroup = display.newGroup()

local submitbutton = display.newRoundedRect(_SCREEN.CENTER.x, _SCREEN.CENTER.y * 1.75, 200, 60, 4)
submitbutton:setFillColor( 151/255, 196/255, 43/255 )

local submittext = display.newText( "Submit", _SCREEN.CENTER.x, _SCREEN.CENTER.y * 1.75, "Helvetica", 18 )
submittext:setTextColor( 1, 1, 1, 255 )

submitgroup:insert(submitbutton)
submitgroup:insert(submittext)
----------------------------------
----------------------------------

-- ADD FUNCTION HERE // FOR WHEN THE SUBMITGROUP HAS BEEN TOUCHED/TAPPED AND THE LISTENER

----------------------------------
----------------------------------
