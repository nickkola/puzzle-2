# Business Diagnostic
## business-diagnostic

> --------------------- ------------------------------------------------------------------------------------------
> __Revision__          [REVISION_LABEL](REVISION_URL)
> __Keywords__          iOS, Android
> __See also__          
> --------------------- ------------------------------------------------------------------------------------------

## Overview

You should start all new projects by copying this Folder Structure.

This structure allows you to:

* develop cross-platform Corona Enterprise apps
* create plugins for Corona Enterprise
* manage under source control all assets.

## Code Walkthrough

### iOS
* Anything specific we care to note

#### A potential Subheading
* Anything specific we care to note

### Android

* Anything specific we care to note
